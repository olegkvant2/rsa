#include <stdio.h>
#include "functions.h"
#include "rsa.h"
#include <locale.h>


int main()
{
	uint64 prime1, prime2;
	uint128 modulus;
	uint128 totient;
	uint64 enc_exp;
	int128 dec_exp, trash1, trash2;
	bool is_prime;
	struct public_key_class pub;
	struct private_key_class priv;
	char message[] = "123abc";
	long long *encrypted;
	char *decrypted;
	int i;

	setlocale(LC_ALL,"Russian");
	prime1 = 101;
	prime2 = 89;

	is_prime = check_prime(prime1);
	if (is_prime) is_prime = check_prime(prime2);
	if (!is_prime)
	{
		printf("���� � ������ �� � �������!");
		return 1;
	}

	modulus = (uint128) prime1 * prime2;
	totient = pxp_lambda(prime1, prime2);
	enc_exp = get_ee(totient);

	eea(enc_exp, totient, &dec_exp, &trash1, &trash2);
	while (dec_exp < 0) dec_exp += totient;

	pub.modulus = modulus;
	pub.exponent = enc_exp;
	priv.modulus = modulus;
	priv.exponent = dec_exp;

	encrypted = rsa_encrypt(message, sizeof(message), &pub);
	if (!encrypted){
		fprintf(stderr, "������� � ������������!\n");
		return 1;
	}
	printf("�����������:\n");
	for(i=0; i < strlen(message); i++){
		printf("%lld\n", (long long)encrypted[i]);
	}  

	decrypted = rsa_decrypt(encrypted, 8*sizeof(message), &priv);
	if (!decrypted){
		fprintf(stderr, "������� � �������������!\n");
		return 1;
	}
	printf("������������:\n");
	for(i=0; i < strlen(message); i++){
		printf("%c\n", decrypted[i]);
	}  

	printf("\n");
	free(encrypted);
	free(decrypted);

	return 0;
}
