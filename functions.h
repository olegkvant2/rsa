#include <limits.h>

#define UINT64_MAX 0xFFFFFFFFFFFFFFFFULL
#define INT64_MAX 0x7FFFFFFFFFFFFFFFLL

typedef unsigned long long uint128;
typedef long long int128;
typedef unsigned long long ull;

#if ULONG_MAX >= UINT64_MAX
typedef unsigned long uint64;
#else
typedef unsigned long long uint64;
#endif

#if LONG_MAX >= INT64_MAX
typedef long int64;
# else
typedef long long int64;
#endif
#define bool	int
#define true	1
#define false	0


uint64 mod_pow(uint64 base, uint128 exponent, uint128 modulus);

uint64 gcd(uint64, uint64);

uint128 lcm(uint64, uint64);

void eea(int128 a, int128 b, int128 *x, int128 *y, int128 *d);

uint128 pxp_lambda(uint64 prime1, uint64 prime2);

bool check_prime(uint64);

bool d_miller_test(uint64);

bool check_composite(uint64 base, uint64 factor, uint64 target, int power);

uint64 get_ee(uint128);
